﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[AddComponentMenu("Control Script/FPS Input")]

public class FPSInput : MonoBehaviour {
    public float characterSpeed;
    public float gravity = 9.8f;
    CharacterController characterController;
	// Use this for initialization
	void Start () {
        characterController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
        float deltaX = Input.GetAxis("Horizontal") * characterSpeed;
        float deltaZ = Input.GetAxis("Vertical") * characterSpeed;
        Vector3 movement = new Vector3(deltaX, 0, deltaZ);

        movement = Vector3.ClampMagnitude(movement, characterSpeed);
        //movement.y = gravity;
        movement *= Time.deltaTime;


        movement = transform.TransformDirection(movement);
        characterController.Move(movement);
	}
}
